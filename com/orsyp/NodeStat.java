package com.orsyp;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

import com.orsyp.util.CsvUtil;
import com.orsyp.util.FileUtil;

public class NodeStat {
	
	public String inputFile;
	public List<List<String>> csvInput = new ArrayList<List<String>>();
	public List<String> nodeList = new ArrayList(); 

	public NodeStat(String nodeStatFile) throws IOException {
		this.inputFile=nodeStatFile;
		removeEmptyLines();
		removeDuplicates();
	}
	
private void removeDuplicates() throws IOException {
	InputStream f = new FileInputStream(this.inputFile);						
	List<List<String>> csvInputTmp=CsvUtil.parseCsv(f);
	List<String> tmpListAgents = new ArrayList<String>();
	
	boolean errorOnDup=false;
	
	// extracting the list of all agents (the only key that should be unique)
	for(int i=0;i<csvInputTmp.size();i++){
		tmpListAgents.add(csvInputTmp.get(i).get(0));
	}
	
	// We keep track of all the indexes that will need to be deleted after the full list is processed
	List<Integer> listIdxToDelete = new ArrayList<Integer>();
	for(int j=0;j<tmpListAgents.size();j++){
		boolean DupFound=false;
		for(int k=0;k<csvInputTmp.size();k++){
			//if the agent name is the same and the idx is NOT the same, then it is necessarily a duplicate
			if(csvInputTmp.get(k).get(0).equals(tmpListAgents.get(j)) && (k!=j)){
				DupFound=true;
				errorOnDup=true;
				//System.out.println(" --- WARNING ! Found duplicated entries for agent "+tmpListAgents.get(j));
				
				// we delete the shortest duplicated line, if same length we delete the max idx: We only keep the longest line (contains more data)
				int sizeLineK=csvInputTmp.get(k).size();
				int sizeLineJ=csvInputTmp.get(j).size();
				
				int idxToDelete=-1;

				if(sizeLineK>sizeLineJ){idxToDelete=j;}
				if(sizeLineJ>sizeLineK){idxToDelete=k;}
				if(sizeLineJ==sizeLineK){idxToDelete=Math.max(k, j);}
				if(!listIdxToDelete.contains(idxToDelete)){listIdxToDelete.add(idxToDelete);}

				
			}
		}
	}
	
	if(errorOnDup){
	List<List<String>> tmpEntries = new ArrayList<List<String>>();
	for(int i=0;i<csvInputTmp.size();i++){
		if(!listIdxToDelete.contains(i)){
			tmpEntries.add(csvInputTmp.get(i));
		}else{
			System.out.println(" %%% INFO : Deleting Found duplicated entries for index "+i+", agent "+tmpListAgents.get(i));
			GoAgt.sayMore(GoAgt.Verbose, " ### DEBUG INFO: line content: "+csvInputTmp.get(i));
		}
	}
	writeFile(this.inputFile,tmpEntries);
	}
	}

public List<List<String>> getCSVStruct() throws IOException{
	checkConsistency();
	writeFile(inputFile,csvInput);
	return csvInput;

}
	public void buildTable() throws FileNotFoundException {
		InputStream f = new FileInputStream(this.inputFile);						
		csvInput=CsvUtil.parseCsv(f);
		GoAgt.sayMore(GoAgt.Verbose, " ### DEBUG INFO: CSV File has: "+csvInput.size());
	}
	
	public void removeEmptyLines() throws IOException{
		InputStream f = new FileInputStream(this.inputFile);						
		List<List<String>> csvInputTmp=CsvUtil.parseCsv(f);
		Iterator it = csvInputTmp.iterator();
		while(it.hasNext()){
			List<String> myList = (List<String>) it.next();
			if(myList.isEmpty()){it.remove();}
		}

		writeFile(inputFile,csvInputTmp);	
	}
	
	public void getNodeNames() throws FileNotFoundException {
		if(csvInput.size() > 1){
			GoAgt.sayMore(GoAgt.Verbose, " ### DEBUG INFO: Csv Entries for Nodes Found");
		for(int k=0;k<csvInput.size();k++){
			if(! csvInput.get(k).isEmpty()){nodeList.add(csvInput.get(k).get(0));}
			
		}
		}else{	GoAgt.sayMore(GoAgt.Verbose, " ### DEBUG INFO: Csv Entries for Nodes NOT Found");}
	}

	public boolean isPresent(String agentName) {
		GoAgt.sayMore(GoAgt.Verbose, " ### DEBUG INFO: checking presence of agent in csv file:["+agentName+"]");
		if(GoAgt.Verbose){
			for(int h=0;h<nodeList.size();h++){
				GoAgt.sayMore(GoAgt.Verbose, " ### DEBUG INFO: Agent:["+nodeList.get(h)+"]");
			}
		}
		GoAgt.sayMore(GoAgt.Verbose, " ### DEBUG INFO: is in List:"+nodeList.contains(agentName));
		return nodeList.contains(agentName);
	}

	public void addItemToFile(String agentProduct, String agentCurrStat, String agentCurrStatName, String agentOS, String agentName, String agentStatus,
			String agentDate) throws IOException {
		List<String> myList = new ArrayList();

		myList.add(0,agentName);
		myList.add(1,agentProduct);
		myList.add(2,agentOS);
		myList.add(3,agentCurrStat);
		myList.add(4,agentCurrStatName);

		myList.add("["+agentStatus+":"+agentDate.replace(" ",":")+"]");
		
		csvInput.add(myList);
		writeFile(inputFile,csvInput);
	}

	public void addItemInFile(String agentName, String agentStatus,
			String agentDate, int MaxElNum) throws IOException {
		
		int idx = nodeList.indexOf(agentName);
		List<String> myList = csvInput.get(idx);
		
		int lgt = myList.size();
		for (int k=lgt;k==4;k--){
			myList.add(k+1, myList.get(k));
		}
		myList.add(5,"["+agentStatus+":"+agentDate.replace(" ",":")+"]");

		if ( myList.size() > MaxElNum+5){	
			myList.remove(myList.size()-1);
		}
		csvInput.set(idx, myList);
		writeFile(inputFile,csvInput);
	}

	public void writeFile(String f,List<List<String>> c) throws IOException{
		File myNewSFile=new File(f);
		if(GoAgt.Verbose){
			GoAgt.sayMore(GoAgt.Verbose," ### DEBUG INFO: WRITING FILE OPERATION: "+f+", File BEFORE contains:");	
			Scanner scanner = new Scanner(myNewSFile);
			while (scanner.hasNextLine()) {
			String line = scanner.nextLine();
			System.out.println(" ### DEBUG INFO:"+line);
		}
			GoAgt.sayMore(GoAgt.Verbose," ### DEBUG INFO: END OF FILE CONTENT");
		}
		
		InputStream sN = CsvUtil.formatCsv(c);
		FileUtil.write(myNewSFile,sN,false);
		if(GoAgt.Verbose){
			GoAgt.sayMore(GoAgt.Verbose," ### DEBUG INFO: WRITING FILE OPERATION: "+f+", File AFTER contains:");	
			Scanner scanner = new Scanner(myNewSFile);
			while (scanner.hasNextLine()) {
			String line = scanner.nextLine();
			System.out.println(" ### DEBUG INFO:"+line);
		}
			GoAgt.sayMore(GoAgt.Verbose," ### DEBUG INFO: END OF FILE CONTENT");
		}
	}

	// if agent is already in the list..
	public void updAgtInFile(String agentName, String agentProduct, String agentStatus, String agentStatDesc,
			String agentOS) throws IOException {
		int idx = nodeList.indexOf(agentName);
		//GoAgt.sayMore(GoAgt.Verbose," ### DEBUG INFO: Index in CSV File:"+idx);
		List<String> myList = csvInput.get(idx);
		myList.set(1,agentProduct);
		myList.set(2,agentOS);
		myList.set(3,agentStatus);
		myList.set(4,agentStatDesc);
		
		csvInput.set(idx, myList);
		if(GoAgt.Verbose){
		GoAgt.sayMore(GoAgt.Verbose," ### DEBUG INFO: Index in CSV File:"+idx+" For:["+agentName+"]["+agentProduct+"]["+agentOS+"]["+agentStatus+"]["+agentStatDesc+"]");
		readCSVFile(csvInput);
		}
		writeFile(inputFile,csvInput);
		
	}
	
	public void readCSVFile(List<List<String>> l){
		
		for(int f=0;f<l.size();f++){
			String t="";
			for (int g=0;g<l.get(f).size();g++){
				t=t+"["+l.get(f).get(g)+"]";
			}
			System.out.println(" ### DEBUG INFO:"+t);
		}
		
		
	}
	
	// if agent has to be added to the list
	public void addAgtToFile(String agentName, String agentProduct, String agentStatus, String agentStatDesc,
			String agentOS) throws IOException {
		GoAgt.sayMore(GoAgt.Verbose," ### DEBUG INFO: Agent Add to CSV File:["+agentName+"]["+agentProduct+"]["+agentStatus+"]["+agentOS+"]");
		nodeList.add(agentName);
		int idxEnd=nodeList.size();
		List<String> myList = new ArrayList();
		myList.add(0,agentName);
		myList.add(1,agentProduct);
		myList.add(2,agentOS);
		myList.add(3,agentStatus);
		myList.add(4,agentStatDesc);
		
		csvInput.add(myList);
		writeFile(inputFile,csvInput);
		
	}

	public List<String> checkNodesDeclared(List<String> UVMSnodeList) {
		List<String> myList = new ArrayList();
		
		for(int i=0;i<nodeList.size();i++){
			if (! UVMSnodeList.contains(nodeList.get(i))){myList.add(nodeList.get(i));}
		}
		return myList;
	}
	public void checkConsistency(){
		
		List<List<String>> TmpCsvInput = new ArrayList<List<String>>();
		//this.csvInput
		for(int f=0;f<this.csvInput.size();f++){
			//line is at this level
			boolean check=false;
			if(this.csvInput.get(f).size()>4){check=true;}
			
			if(check){
				TmpCsvInput.add(this.csvInput.get(f));
			}else{
				System.out.println(" !!! Warning: Line: "+this.csvInput.get(f)+" is inconsistent... ignoring");
			}
			
		}
		this.csvInput=TmpCsvInput;
	}

}

