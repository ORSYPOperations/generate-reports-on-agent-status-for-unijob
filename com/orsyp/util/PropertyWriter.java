package com.orsyp.util;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Properties;
import java.util.ResourceBundle;

// ----------------------------------------------------------------------------
/**
 * A simple class for writing Properties to file
 */
public
abstract class PropertyWriter
{

    public static void saveProperties (Properties pr, String fileName, String delim) throws IOException
    {

    	BufferedWriter buffer = new BufferedWriter(new FileWriter(fileName));

    	buffer.flush();
    	for (Enumeration keys = pr.keys(); keys.hasMoreElements ();)
        {
            final String key = (String) keys.nextElement ();
            final String value = pr.get(key).toString();
            buffer.write(key+delim+value);
        } 

    	buffer.close();
    }
    
    public static void saveHashMap (String fileName, HashMap hashmap, String delim) throws IOException
    {

    	BufferedWriter buffer = new BufferedWriter(new FileWriter(fileName));

    	buffer.flush();
    	for (Iterator<String> it = hashmap.keySet().iterator() ; it.hasNext() ; )
        {
        	final String key = (String) it.next();
			final String value = hashmap.get(key).toString();
			buffer.write(key+delim+value);
            buffer.newLine();
        } 

    	buffer.close();
    }
    
 

} // end of class
// ----------------------------------------------------------------------------