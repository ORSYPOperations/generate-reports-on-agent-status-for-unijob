package com.orsyp.util;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Map;
import java.util.StringTokenizer;

public class WordCount {
	  static final Integer ONE = new Integer(1);
	  static final String ERROR_PATTERN_1 = "command in error";
	  static final String ERROR_PATTERN_2 = "Error :";

  public static int CountErrors(String file) throws IOException {

    Hashtable map = new Hashtable();
    FileReader fr = new FileReader(file);
    BufferedReader br = new BufferedReader(fr);
    String line;
    while ((line = br.readLine()) != null) {
      processLine(line, map);
    }
    return map.size();
  }

  static void processLineOld(String line, Map map) {
    StringTokenizer st = new StringTokenizer(line);
    while (st.hasMoreTokens()) {
      addWord(map, st.nextToken());
    }
  }
  
  static void processLine(String line, Map map) {
	    if (line.startsWith(ERROR_PATTERN_1) || line.startsWith(ERROR_PATTERN_2))
	      addWord(map, "ERROR"+map.size());
	  }
  
  

  static void addWord(Map map, String word) {
    Object obj = map.get(word);
    if (obj == null) {
      map.put(word, ONE);
    } else {
      int i = ((Integer) obj).intValue() + 1;
      map.put(word, new Integer(i));
    }
  }
}