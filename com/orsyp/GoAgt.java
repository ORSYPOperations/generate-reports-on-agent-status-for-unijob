package com.orsyp;


import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Vector;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.data.JRCsvDataSource;
import com.orsyp.std.central.network.NetworkNodeListStdImpl;
import com.orsyp.unijob.UVMSInterface;
import com.orsyp.util.PropertyLoader;
import com.orsyp.util.CsvUtil;
import com.orsyp.util.FileUtil;
import com.orsyp.api.Context;
import com.orsyp.api.central.UniCentral;
import com.orsyp.api.central.UniCentral.LicenseEntryStatus;
import com.orsyp.api.central.networknode.NetworkNodeItem;
import com.orsyp.api.central.networknode.NetworkNodeList;
import com.orsyp.api.central.networknode.NetworkNodeId.PRODUCT_TYPE;
import com.orsyp.api.central.networknode.NodeCompanyConfiguration;
import com.orsyp.help.*;
import com.orsyp.kmeleon.configuration.NodeConfiguration;
import edu.mscd.cs.jclo.JCLO;


/**
 * INTERNAL RELEASE NOTES:
 * 
 * 1.0.7: consistency check based on the number of entries per line and the detection of duplicates
 * 1.0.8: handling of duplicate entries in csv file
 * 1.0.9: fixing an issue where the node list returned by the UVMS is not correct
 * Dec 2013: Adaptation to UV6 and remove API Checks
 **/

class AllArgs {
    private boolean help;
    private String pwd;
    private String login;
    private int port;
    private String uvms;
    private boolean version;
    private boolean verbose;
    private boolean u;
    private boolean r;
    private boolean s;
    private String agent;
    private String status;
    private String filter;
    private String date;
}

public class GoAgt {

	//static variables
	
	static boolean Verbose;
	static String Customer = "CLSA";
	static String Release="1.0.9";
	static String[] APIversions = {"300"};
	static String DefaultFilter="NZXCFBSYUIWM";
	
	static String[] csvColumnNamesB = new String[]{"AGENT", "PRODUCT", "OS"
		,"STATUS CODE", "STATUS", "EVENTA", "EVENTB", "EVENTC", "EVENTD", "EVENTE"};
	
	static String[] csvColumnNamesO = new String[]{"STATUS", "NUMBER", "PERCENTAGE"};
	
	static String[] statusNames = {"BEING COMPUTED", "UNKNOWN", "CONNECTING"
		,"CONNECTED", "FAILED", "BROKEN", "STOPPED", "UNREACHABLE", "NODE UNREACHABLE"
		,"IO UNREACHABLE", "CONNECTED WITH WARNINGS", "NO UPWARDS MESSAGE"};
	
	static int[] statusCount = {0,0,0,0,0,0,0,0,0,0,0,0};
	static int[] statusPercentage = {0,0,0,0,0,0,0,0,0,0,0,0};
	static String csvDelim="\r\n";
	

	public static void main(String[] argv) throws IOException, InterruptedException {
			
		Collection<String> colFILTER= new Vector<String>();
		Collection<Integer> colSTATUS= new Vector<Integer>();
		Collection<String> colSTATUSNAME= new Vector<String>();
		
		List<String> UVMSnodeList = new ArrayList<String>(); 
		
		List<List<String>> csvInput = new ArrayList<List<String>>();
		List<List<String>> csvInputO = new ArrayList<List<String>>();
		
		String password="";
		String login="";
		String UVMSHost="";
		String StatFlag="";
		// checking for a minimum number of nodes is certainly not clean... but there is no other way
		int DefMinNumberOfNodes=22;
		int DefDelayBeforeRetry=600000;
		int DefMaxNumAttempts=10;
		int MinNumberOfNodes=0;
		int DelayBeforeRetry=0;
		int MaxNumAttempts=0;
		int port=0;
		boolean displayHelp=false;
		boolean displayVer=false;
		boolean verbose=false;
		boolean updateFlag=false;
		boolean genReports=false;
		boolean statusUpd=false;
		String agentName="";
		String agentStatus="";
		String agentDate="";

		try {
			
			System.out.println("=======================================================");
			System.out.println("  ** ORSYP Unijob Custom Reporting Module v  " + Release +" ** ");
			System.out.println("  *          ORSYP Professional Services.           * ");
			System.out.println("  * Copyright (c) 2011 ORSYP.  All rights reserved. * ");
			System.out.println("  **             Implemented for "+Customer+"              **");
			System.out.println("=======================================================");
			System.out.println("");
			
		        JCLO jclo = new JCLO (new AllArgs());
		        jclo.parse (argv);
		        
		        displayHelp=jclo.getBoolean ("help");
		        displayVer=jclo.getBoolean ("version");
		        password=jclo.getString ("pwd");
		        login=jclo.getString ("login");
		        UVMSHost=jclo.getString ("uvms");
		        port=jclo.getInt ("port");
		        verbose=jclo.getBoolean ("verbose");
		        StatFlag=jclo.getString ("filter");
		        displayVer=jclo.getBoolean ("version");
		        updateFlag=jclo.getBoolean("u");
		        genReports=jclo.getBoolean("r");
		        statusUpd=jclo.getBoolean("s");
		        agentName=jclo.getString("agent");
		        agentStatus=jclo.getString("status");
		        agentDate=jclo.getString("date");
		        
		        if(verbose){Verbose=true;}

		        if(displayHelp){OnlineHelp.displayHelp(0,"Display help",Release);}
		        if(displayVer){OnlineHelp.displayVersion(0,Release,APIversions);}
			    
		        Properties pGlobalVariables = PropertyLoader.loadProperties("UJAgtMgt");
			   
		        String FileB=pGlobalVariables.getProperty("INPUT_BREAKDOWN");
			    String FileO=pGlobalVariables.getProperty("INPUT_OVERVIEW");
			    // MinNumberOfNodes=Integer.parseInt(pGlobalVariables.getProperty("MIN_NUMBER_OF_NODES"));
			    
		        if (password==null || password.equals("")){password=pGlobalVariables.getProperty("UVMS_PWD");}    
			    if (password==null || password.equals("")){OnlineHelp.displayHelp(-1,"Error: No Password Passed!",Release);}

			   
			    
				System.out.println("==> Loading Program Options ...");
	            
				try{
				MaxNumAttempts=Integer.parseInt(pGlobalVariables.getProperty("MAX_NUM_OF_ATTEMPTS"));
				}catch (NumberFormatException e){
					MaxNumAttempts=DefMaxNumAttempts;
				}
				
				try{
				DelayBeforeRetry=60000*Integer.parseInt(pGlobalVariables.getProperty("DELAY_BETWEEN_ATTEMPTS"));
				}catch (NumberFormatException e){
					DelayBeforeRetry=DefDelayBeforeRetry;
				}
				
				// Testing for the presence of overwriting parameter in properties file..
				try{
					MinNumberOfNodes=Integer.parseInt(pGlobalVariables.getProperty("MIN_NUMBER_OF_NODES"));
				}catch (NumberFormatException e){
					MinNumberOfNodes=DefMinNumberOfNodes;
				}
				//if (MinNumberOfNodes==0){}
	            if (login == null || login.equals("")){login=pGlobalVariables.getProperty("UVMS_USER");}
	            if (UVMSHost == null || UVMSHost.equals("")){UVMSHost=pGlobalVariables.getProperty("UVMS_HOST");}
	            if (port==0){port=Integer.parseInt(pGlobalVariables.getProperty("UVMS_PORT"));}
	            
	            if(UVMSHost==null||password==null||login==null||UVMSHost==""||port==0||password==""||login==""){
	            	System.out.println("-- Fatal Error in Parameters Passed.");
	            	OnlineHelp.displayHelp(1,"Error in Parameters",Release);
	            }
	            

				UVMSInterface uvInterface = new UVMSInterface(UVMSHost,port,login,password,pGlobalVariables.getProperty("DU_NODE"),pGlobalVariables.getProperty("DU_COMPANY"),Area.Exploitation);
				
				UniCentral central = uvInterface.ConnectEnvironment();
				boolean supVersion=false;
				for (int k=0;k<APIversions.length;k++){
					if (String.valueOf(central.getVersion()).equals(APIversions[k])){
						supVersion=true;
					}
				}
				if (! supVersion){
					//System.out.println("   --- Warning ! Current Version of UVMS ["+central.getVersion()+"] does not seem to be supported.");
					//System.out.println("   --- Program might not work as expected: Please Check Version Compatibility with option -v...\n");
					}
				boolean checkLicActive=false;
				if(checkLicActive){
	            // Check for Licence Validity Here !!
				
				String LicStr=pGlobalVariables.getProperty("REPORTER_LIC");
				boolean RepLicOK=false;
				String LicStat="";
				boolean RepLicFound=false;
				if (LicStr==null || LicStr.equals("")){
				 sayMore(verbose," ### DEBUG INFO: No Licence Found in Properties File.");
				 try{
						String RootDir = central.getCentralConfigurationVariable("UNI_DIR_ROOT").replace("\\", "/");
						String LicFile=RootDir+"/data/licenses.txt";

					  FileInputStream fstream = new FileInputStream(LicFile);
					  DataInputStream in = new DataInputStream(fstream);
					  BufferedReader br = new BufferedReader(new InputStreamReader(in));
					  String strLine;
					  
					  while ((strLine = br.readLine()) != null)   {
						  if(strLine.contains("REPORTER")){
							  RepLicFound=true;
						  LicenseEntryStatus myLicStat= central.checkLicenseEntry(strLine);
						  if(myLicStat.toString().equals("VALID")){
							  RepLicOK=true;
							  LicStat=myLicStat.toString();
						  }else{LicStat=myLicStat.toString();}
						  }
						  
					  }
					  in.close();
					  
					    }catch (Exception e){
					  System.err.println("Error: " + e.getMessage());
					  }
				}else{
					sayMore(verbose," ### DEBUG INFO: Licence Found in Properties File:"+LicStr);
					RepLicFound=true;
					LicenseEntryStatus myLicStat= central.checkLicenseEntry(LicStr);  
					  if(myLicStat.toString().equals("VALID")){
						  RepLicOK=true;
						  LicStat=myLicStat.toString();
					  }else{LicStat=myLicStat.toString();}	  
						  
					  }
				if(!RepLicFound){
					System.out.println("  --- ERROR - No Licence for REPORTER Found on UVMS.");
					System.exit(99);
				}
				if(!RepLicOK){
					System.out.println("  --- ERROR - Licence for REPORTER on UVMS has Status: "+LicStat);
					System.exit(99);
				}
				sayMore(verbose," ### DEBUG INFO: Licence Found:"+RepLicFound);
				sayMore(verbose," ### DEBUG INFO: Licence Status:"+LicStat);	
				}
				
				NetworkNodeList networkList = getNodeList(uvInterface.getContext());
				//NetworkNodeList networkList = new NetworkNodeList(uvInterface.getContext());
				sayMore(verbose,"    ### DEBUG INFO: Number of Nodes: "+networkList.getCount());
				int ct=0;
				while(networkList.getCount()<MinNumberOfNodes && ct < MaxNumAttempts){
					System.out.println("  %%% WARNING: Number of Agents Returned is insufficient: "+networkList.getCount());
					System.out.println("  %%%          Activating DEBUG Mode & Sleeping for "+DelayBeforeRetry/60000+" minutes before retrying.");
					for(int i=0;i<networkList.getCount();i++){
						sayMore(verbose,"    ### DEBUG INFO: Node List: "+networkList.get(i).getName());
					}
					Thread.sleep(DelayBeforeRetry);
					verbose=true;
					networkList = getNodeList(uvInterface.getContext());
					ct++;
				}
				if(ct==MaxNumAttempts && networkList.getCount()<MinNumberOfNodes){
					System.out.println("  --- ERROR:  Max Number of Attempts Reached: "+MaxNumAttempts);
					System.out.println("  --- ERROR:  Number of Agents Returned is insufficient: "+networkList.getCount()+ ", Min is:" + MinNumberOfNodes);
					for(int i=0;i<networkList.getCount();i++){
						sayMore(true,"    ### DEBUG INFO: Node List: "+networkList.get(i).getName());
					}
					System.exit(99);
				}

				if ( StatFlag == null ){StatFlag=DefaultFilter;}
				
			
				
				
	            String dUJ="";
	            String dDU="";
	            String dUVMS="";
	            String dREP="";
	            String dPUB="";
	            
	            dUJ=pGlobalVariables.getProperty("DISPLAY_UNIJOB");
	            dDU=pGlobalVariables.getProperty("DISPLAY_DU");
	            dUVMS=pGlobalVariables.getProperty("DISPLAY_UVMS");
	            dREP=pGlobalVariables.getProperty("DISPLAY_REPORTER");
	            dPUB=pGlobalVariables.getProperty("DISPLAY_PUBLISHER");
	            
	            if (dUJ.equalsIgnoreCase("Y")){colFILTER.add("UNIJOB");}
	            if (dDU.equalsIgnoreCase("Y")){colFILTER.add("DU");}
	            if (dUVMS.equalsIgnoreCase("Y")){colFILTER.add("CENTRAL");}
	            if (dREP.equalsIgnoreCase("Y")){colFILTER.add("REPORTER");}
	            if (dPUB.equalsIgnoreCase("Y")){colFILTER.add("PUBLISHER");}
	           
				GoAgt.sayMore(GoAgt.Verbose," ### DEBUG INFO: COLL FILTER ON PROD: UJ["+dUJ+"] DU["+dDU+"] UVMS["+dUVMS+"] REPPUB["+dREP+dPUB+"]");
	            //NZXCFBSYUIWM
	            if ( StatFlag != null && StatFlag.contains("N")){colSTATUS.add(-2); colSTATUSNAME.add(statusNames[0]);}
	            if ( StatFlag != null && StatFlag.contains("Z")){colSTATUS.add(-1); colSTATUSNAME.add(statusNames[1]);}
	            if ( StatFlag != null && StatFlag.contains("X")){colSTATUS.add(0); colSTATUSNAME.add(statusNames[2]);}
	            if ( StatFlag != null && StatFlag.contains("C")){colSTATUS.add(1); colSTATUSNAME.add(statusNames[3]);}
	            if ( StatFlag != null && StatFlag.contains("F")){colSTATUS.add(2); colSTATUSNAME.add(statusNames[4]);}
	            if ( StatFlag != null && StatFlag.contains("B")){colSTATUS.add(3); colSTATUSNAME.add(statusNames[5]);}
	            if ( StatFlag != null && StatFlag.contains("S")){colSTATUS.add(4); colSTATUSNAME.add(statusNames[6]);}
	            if ( StatFlag != null && StatFlag.contains("Y")){colSTATUS.add(5); colSTATUSNAME.add(statusNames[7]);}
	            if ( StatFlag != null && StatFlag.contains("U")){colSTATUS.add(6); colSTATUSNAME.add(statusNames[8]);}
	            if ( StatFlag != null && StatFlag.contains("I")){colSTATUS.add(7); colSTATUSNAME.add(statusNames[9]);}
	            if ( StatFlag != null && StatFlag.contains("W")){colSTATUS.add(8); colSTATUSNAME.add(statusNames[10]);}
	            if ( StatFlag != null && StatFlag.contains("M")){colSTATUS.add(9); colSTATUSNAME.add(statusNames[11]);}

	            GoAgt.sayMore(GoAgt.Verbose," ### DEBUG INFO: COLL FILTER ON STATUS:"+colSTATUS.toString());
	            
	            sayMore(verbose," ### DEBUG INFO: Status Flags are:"+StatFlag);
	            sayMore(verbose," ### DEBUG INFO: Status Kept are:"+colSTATUSNAME);
	        	
	            if (! updateFlag){
					if (statusUpd){
						System.out.println("==> Status Update ...");
				/**
				 * Retrieve the node list from UVMS, all parameters can be seen below, however we only use some
				 * and we filter on Unijob Nodes only
				 */

	            System.out.println(" --> List of elements retrieved:");
	            	String myHostname="";
	            	int myPort=0;
	            	NodeStat NodeStatTable = new NodeStat(FileB);
					NodeStatTable.buildTable();
					NodeStatTable.getNodeNames();
	            	sayMore(verbose," ### DEBUG INFO: Agent List has: "+networkList.getCount()+" Elements");
	            	if(verbose){
	            	for (int i = 0; i < networkList.getCount(); i++) {
	            		sayMore(verbose," ### DEBUG INFO: AGENT LIST:"+networkList.get(i).getName()+
	            				":"+networkList.get(i).getType()+":"+networkList.get(i).getNodeInfo().getStatus());
	            	}
	            	}
	            	
					for (int i = 0; i < networkList.getCount(); i++) {
						NetworkNodeItem item = networkList.get(i);
						String NodeName=item.getNodeCompanyConfiguration().getNodeName();
						PRODUCT_TYPE ProdType=item.getNodeCompanyConfiguration().getProductType();
						NodeCompanyConfiguration myCompConf=item.getNodeCompanyConfiguration();
						NodeConfiguration myConf = new NodeConfiguration(Area.Exploitation,myCompConf);
						
						// Retrieving Hostame
						myHostname=myConf.getHostname();
						// Retrieving Port Number
		
			Map <String,String> portMap;
			portMap=myConf.getNodeCompanyConfiguration().getAreaConfiguration(Area.Exploitation.toString()).getServerCodes();
			if (portMap.containsKey("REP")){myPort=Integer.valueOf(portMap.get("REP"));}
			if (portMap.containsKey("PUB")){myPort=Integer.valueOf(portMap.get("PUB"));}

						UVMSnodeList.add(NodeName);
						if (colFILTER.contains(ProdType.toString())){
							int NodeStat;
							if (ProdType.toString().equals("REPORTER") || ProdType.toString().equals("PUBLISHER")){
								
								String myHOST="";
								int myPORT=0;
								sayMore(verbose," ### DEBUG INFO: ProdType is:"+ProdType.toString());
								sayMore(verbose," ### DEBUG INFO: REP HOST is:"+myHostname);
								sayMore(verbose," ### DEBUG INFO: REP PORT is:"+myPort);

									myHOST=myHostname;
									myPORT=myPort;

								if(isRepPubOnline(verbose, myHOST, myPORT)){NodeStat=1;}else NodeStat=2;
								
							}else{
							NodeStat = item.getNodeInfo().getStatus();
							}
							if (colSTATUS.contains(NodeStat)){
						
								/**
								 * Possible Statuses:
								 * 
								 * -2: Being Computed / Status is being computed
								 * -1: Unknown / Status is yet to be computed
								 * 0 : Connecting / client or server connecting to an environment
								 * 1 : Connected / Normal status
								 * 2 : Failed / client or server has failed to connect to env
								 * 3 : Broken / Client or server has connected to env but connection is broken (no more collection of info from this env)
								 * 4 : Stopped / client or server has stopped collecting info from env
								 * 5 : Unreachable / Environment is unreachable
								 * 6 : Node Unreachable / Node is unreachable
								 * 7 : IO Unreachable / IO service unreachable but node is up 
								 * 8 : Connected with Warnings / client or server connected to IO server, but another engine is not started correctly
								 * 9 : No Upward Message / IO is reachable, but no message is coming from it
								 */
						
						System.out.println("   "
									+NodeName
									+ "," + ProdType
									+ "," + item.getNodeInfo().getOs()
									+ "," + NodeStat
									+ "," + getStatusDesc(NodeStat));

						// Creating the File containing all statuses breakdown first
						
						//NodeStatTable.buildTable();
						//NodeStatTable.getNodeNames();

						boolean isInList = NodeStatTable.isPresent(NodeName);

						if (isInList){
							sayMore(verbose," ### DEBUG INFO: Agent Update, Agent Found in CSV file: "+NodeName);
							NodeStatTable.updAgtInFile(NodeName,ProdType.toString(),String.valueOf(NodeStat),getStatusDesc(NodeStat),item.getNodeInfo().getOs());
						}else{
							sayMore(verbose," ### DEBUG INFO: Agent Update, Agent NOT Found in CSV file: "+NodeName);
							NodeStatTable.addAgtToFile(NodeName,ProdType.toString(),String.valueOf(NodeStat),getStatusDesc(NodeStat),item.getNodeInfo().getOs());
						}
						}
						
						}
					}

					System.out.println(" +++ Node Statuses Updated successfully.");
					NodeStat NodeStatTableT = new NodeStat(FileB);
					NodeStatTableT.buildTable();
					NodeStatTableT.getNodeNames();
					csvInput=NodeStatTable.getCSVStruct();

					// Adding a check for Nodes declared in csv files and not present on UVMS

					List<String> notInUVMS = new ArrayList<String>();
					notInUVMS = NodeStatTableT.checkNodesDeclared(UVMSnodeList);
					
					if (! notInUVMS.isEmpty()){					
							System.out.println("\n --- WARNING !! the following list of Agents is no longer declared in UVMS: "+notInUVMS.toString());
					}

					int numObj=csvInput.size();
					sayMore(verbose," ### DEBUG INFO: Number of Lines in csv: "+numObj);
					if(numObj==0){System.out.println("  --- ERROR! CSV file seems empty..."); System.exit(1);}
			
					
					
					List<String> tmpList = new ArrayList<String>();
					
					for (int h=0;h<csvInput.size();h++){
						tmpList=csvInput.get(h);
						int chkLength=tmpList.size();
						if(chkLength<4){
							System.out.println("\n --- WARNING !! the following line is inconsistent, please check: "+tmpList.toString());
						}else{
						int tmpCnt=Integer.valueOf(tmpList.get(3));
						int tmpNum=statusCount[tmpCnt+2]+1;
						statusCount[tmpCnt+2]=tmpNum;
						}
					}
					
					for (int k=0;k<statusCount.length;k++){
						statusPercentage[k]=(100 * statusCount[k])/numObj;
					}

					for (int k=0;k<statusNames.length;k++){
						List<String> myTmpList = new ArrayList<String>();
						myTmpList.add(statusNames[k]);
						myTmpList.add(String.valueOf(statusCount[k]));
						myTmpList.add(String.valueOf(statusPercentage[k]));
						if(statusCount[k]!=0){csvInputO.add(myTmpList);}
					}
	
					File myNewSFileO=new File(FileO);
					InputStream sNO = CsvUtil.formatCsv(csvInputO);
					FileUtil.write(myNewSFileO,sNO,false);
					}
					
					if (genReports){
						System.out.println("\n==> Report Generation Activated :");

					File myNewSFileO=new File(FileO);
					File myNewSFileB=new File(FileB);
					
					InputStream fO = new FileInputStream(myNewSFileO);
					InputStream fB = new FileInputStream(myNewSFileB);

					List<List<String>> csvInputTmpO = new ArrayList<List<String>>();
					List<List<String>> csvInputTmpB = new ArrayList<List<String>>();
					csvInputTmpO=CsvUtil.parseCsv(fO);
					csvInputTmpB=CsvUtil.parseCsv(fB);
					
					List<List<String>> csvInputFinO = new ArrayList<List<String>>();
					List<List<String>> csvInputFinB = new ArrayList<List<String>>();
					
					for (int c1=0;c1 < csvInputTmpO.size();c1++){
						// Mar 2012 / fix: Overview Report does not need to filter Statuses
						//if (colSTATUSNAME.contains(csvInputTmpO.get(c1).get(0))){
							csvInputFinO.add(csvInputTmpO.get(c1));
						//}
					}

					for (int c2=0;c2 < csvInputTmpB.size();c2++){
							if (colSTATUSNAME.contains(csvInputTmpB.get(c2).get(4))){
								csvInputFinB.add(csvInputTmpB.get(c2));
								
						}
					}
					if (!StatFlag.equals(DefaultFilter)){
						System.out.println("\n %%% Filtering Activated, ONLY the following Statuses will be processed:");
						System.out.println(" " + colSTATUSNAME + "\n");
					}
					
					if(csvInputFinO.isEmpty()){System.out.println(" !!! No Agent is currently in specified Statuses, Overview Report is empty: "+colSTATUSNAME);}
					if(csvInputFinB.isEmpty()){System.out.println(" !!! No Agent is currently in specified Statuses, Breakdown Report is empty: "+colSTATUSNAME+"\n");}
						
					fO=CsvUtil.formatCsv(csvInputFinO);
					fB=CsvUtil.formatCsv(csvInputFinB);
			
					JRCsvDataSource inCsvO = new JRCsvDataSource(fO);
					JRCsvDataSource inCsvB = new JRCsvDataSource(fB);
					
				    inCsvO.setRecordDelimiter(csvDelim);
				    inCsvB.setRecordDelimiter(csvDelim);
				    inCsvO.setColumnNames(csvColumnNamesO);
				    inCsvB.setColumnNames(csvColumnNamesB);    

				    Calendar calendar = Calendar.getInstance();
			        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd_HHmmss");
			        String Tstamp=dateFormat.format(calendar.getTime()).toString();
				    
				    String OReport = pGlobalVariables.getProperty("REP_OUTPUT_PATH")
				    	+ pGlobalVariables.getProperty("OVERVIEW_REPORT_TEMPLATE").split("\\.")[0]
				    	+ "_"+Tstamp+".pdf";
				    
				    String BReport = pGlobalVariables.getProperty("REP_OUTPUT_PATH")
				    	+ pGlobalVariables.getProperty("BREAKDOWN_REPORT_TEMPLATE").split("\\.")[0]
				    	+ "_"+Tstamp+".pdf";
				    
				    System.out.println(" --> Generating Report: "+OReport);
				    
					JasperExportManager.exportReportToPdfFile(
							JasperFillManager.fillReportToFile(pGlobalVariables.getProperty("REP_TEMPLATE_INPUT_PATH")+
									pGlobalVariables.getProperty("OVERVIEW_REPORT_TEMPLATE")
							, new HashMap<Object, Object>(), inCsvO),OReport); 
					
					System.out.println(" --> Generating Report: "+BReport+"\n");
					
					JasperExportManager.exportReportToPdfFile(JasperFillManager.fillReportToFile(pGlobalVariables.getProperty("REP_TEMPLATE_INPUT_PATH")+
							pGlobalVariables.getProperty("BREAKDOWN_REPORT_TEMPLATE")
							, new HashMap<Object, Object>(), inCsvB),BReport); 
					
					System.out.println(" +++ REPORTS GENERATED SUCCESSFULLY");
					}
			    }
			    else {
			    	
			    	System.out.println("==> Event Update Mode ");

					if (agentName==""||agentStatus==""||agentDate==""){
						System.out.println("-- Fatal Error in Parameters Passed [ 3 needed ].");
						System.exit(1);
					}
					
			    	boolean wasFound=false;
			    	PRODUCT_TYPE agentProduct = null;
			    	int agentCurrStat=-9;
			    	String agentCurrStatName="";
			    	String agentOS="";
			    	
			    	for (int i = 0; i < networkList.getCount(); i++) {
						NetworkNodeItem item = networkList.get(i);
						if(agentName.equals(item.getNodeCompanyConfiguration().getNodeName())){
						agentProduct=item.getNodeCompanyConfiguration().getProductType();
						agentCurrStat = item.getNodeInfo().getStatus();
						agentCurrStatName = getStatusDesc(agentCurrStat);
						agentOS = item.getNodeInfo().getOs();
						wasFound=true;
						}
			    	}
			    	System.out.println("   --> Getting Current Status for Agent: " + agentName);
			    	if (!wasFound){
			    		System.out.println( "---Error: Agent "+agentName+" does not seem to be declared on the Management Server!");
			    		System.exit(1);
			    	}
					
					int MAX_EL=Integer.parseInt(pGlobalVariables.getProperty("MAX_NUMBER_OF_EL_PER_AGT"));
					NodeStat NodeStatTable = new NodeStat(FileB);
					NodeStatTable.buildTable();
					NodeStatTable.getNodeNames();
			    	
					boolean isInList = NodeStatTable.isPresent(agentName);
				
					System.out.println("   --> Updating Status & List of Events ["+agentStatus+"] ["+agentDate+"] for Agent: " + agentName);
					if (isInList){
						NodeStatTable.addItemInFile(agentName,agentStatus,agentDate,MAX_EL);
					}else
						NodeStatTable.addItemToFile(agentProduct.toString(),String.valueOf(agentCurrStat),agentCurrStatName,agentOS, agentName,agentStatus,agentDate);
					
			    }
					System.out.println(" +++ Update Successful.");
					System.out.println("\n==> End of Execution ...");
					System.exit(0);

			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
	}
	
	private static NetworkNodeList getNodeList(Context context) {
		NetworkNodeList n = new NetworkNodeList(context);
		n.setImpl(new NetworkNodeListStdImpl());
		try {
			n.extract();
		} catch (UniverseException e1) {
			e1.printStackTrace();
			return n;
		}
		return n;
	}

	public static boolean isRepPubOnline(boolean v, String host, int port) throws IOException{
        Socket mySoc = null;
        boolean res=false;
		try {
			sayMore(v," ### DEBUG INFO: Initializing Rep Pub socket");
			mySoc = new Socket(host,port);
			if(mySoc.isConnected()==true){res=true;}
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			return false;
		}
		mySoc.close();
		sayMore(v," ### DEBUG INFO: Socket Closed");
		return res;
	}
			
public static boolean convertOption(String s){
	if (s.equalsIgnoreCase("O") || s.equalsIgnoreCase("Y")){
		return true;
	}
	return false;
}

public static String getStatusDesc(int i){
	String Desc = "";
	Desc=statusNames[i+2];
	return Desc;
}

public static void sayMore(boolean v, String myMsg){
	if(v){
		System.out.println(myMsg);
	}
	
}

}