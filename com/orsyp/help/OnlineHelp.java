package com.orsyp.help;

public class OnlineHelp {
	public static void displayHelp(int RC, String Message, String Release) {
		
		System.out.println(Message+"\n");
		System.out.println("-----------------------------------------------------------------------------------");
		System.out.println("   **               ORSYP Custom Reporting Module for CLSA "+Release);
		System.out.println("      This tool keeps track of agent statuses in UVMS and handles reporting         ");		
		System.out.println("-----------------------------------------------------------------------------------\n");
		System.out.println("");
		System.out.println("++ Manual Mode Parameters:\n");
		System.out.println("    --s:          Update all Agent Statuses in Database [ can be used with --r option in order to generate Reports ]");
		System.out.println("    --r:          Generate Reports from Database [ can be used with --s option to get latest statuses in Reports ]");
		System.out.println("    --filter:     Filter only on some statuses, it should be a combination of the following successive Letters: ");
		System.out.println("				  N -> BEING COMPUTED");
		System.out.println("				  Z -> UNKNOWN");
		System.out.println("				  X -> CONNECTING");
		System.out.println("				  C -> CONNECTED");
		System.out.println("				  F -> FAILED");
		System.out.println("				  B -> BROKEN");
		System.out.println("				  S -> STOPPED");
		System.out.println("				  Y -> UNREACHABLE");
		System.out.println("				  U -> NODE UNREACHABLE");
		System.out.println("				  I -> IO UNREACHABLE");
		System.out.println("				  W -> CONNECTED WITH WARNINGS");
		System.out.println("				  M -> NO UPWARDS MESSAGE");
		System.out.println("    Example: --filter=NZXFBSYUIWM will select any Status but filter out CONNECTED [C]");
		System.out.println("");
		System.out.println("++ Auto Mode Parameters [Do Not Trigger Manually]:\n");
		System.out.println("    --u:     add an process event to Database [agent becomes unreachable, or reachable etc.]");
		System.out.println("     	   --agent= :     Agent Name on which to add an event in DB");
		System.out.println("     	   --status=:    Status of the event [STARTED|STOPPED|UNKNOWN]");
		System.out.println("     	   --date=  :      date of event [YYYYMMDD HHmm]");
		System.out.println("");
		System.out.println("++ Display, Help & Version Info:\n");
		System.out.println("    --help:    Display online help");
		System.out.println("    --version: Version Information & UVMS compatibility");
		System.out.println("");
		System.out.println("++ Examples:\n");
		System.out.println("    UJAgtMgt --s");
		System.out.println("    UJAgtMgt --s --r");
		System.out.println("    UJAgtMgt --u --agent=bsaorshk01 --status=STOPPED --date=\"20120101 1200\"");
		System.out.println("    UJAgtMgt --u --agent=bsaorshk01 --status=STOPPED --date=\"20120101 1200\" --filter=NZXFBSYUIWM");
		System.out.println("");
		System.out.println("  IMPORTANT NOTE:");
		System.out.println("  => All Configuration parameters can be modified from properties file in same folder as Jar file");
		System.out.println("");
		System.exit(RC);
	}
	
	/**
	 * Possible Statuses:
	 * 
	 * -2: Being Computed / Status is being computed
	 * -1: Unknown / Status is yet to be computed
	 * 0 : Connecting / client or server connecting to an environment
	 * 1 : Connected / Normal status
	 * 2 : Failed / client or server has failed to connect to env
	 * 3 : Broken / Client or server has connected to env but connection is broken (no more collection of info from this env)
	 * 4 : Stopped / client or server has stopped collecting info from env
	 * 5 : Unreachable / Environment is unreachable
	 * 6 : Node Unreachable / Node is unreachable
	 * 7 : IO Unreachable / IO service unreachable but node is up 
	 * 8 : Connected with Warnings / client or server connected to IO server, but another engine is not started correctly
	 * 9 : No Upward Message / IO is reachable, but no message is coming from it
	 */

	public static void displayVersion(int RC, String release, String[] APIversions) {
		System.out.println("Version "+release);
		System.out.println("++ This Tool was compiled and tested with the following UVMS:");
		for (int j=0;j < APIversions.length;j++){
			System.out.println(APIversions[j]);
		}
		System.out.println("  => Other versions of UVMS might not work as expected or might not work at all");
		System.exit(RC);
	}
}
